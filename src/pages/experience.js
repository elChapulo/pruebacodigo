import React from 'react';

class NameForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = { value: '' };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(event) {
        this.setState({ value: event.target.value });
    }

    handleSubmit(event) {
        alert('A name was submitted: ' + this.state.value);
        event.preventDefault();
    }

    render() {
        return (
            <div className="bg-info">
                <div className="card container pa-3">
                    <form onSubmit={this.handleSubmit}>
                        <div className="form-group">
                            <label for="exampleFormControlInput1">Name:</label>
                            <input className="form-control" id="exampleFormControlInput1" type="text" value={this.state.value} onChange={this.handleChange} />
                        </div>
                        <div className="form-group">
                            <label for="exampleFormControlInput1">Name:</label>
                            <input className="form-control" id="exampleFormControlInput1" type="text" value={this.state.value} onChange={this.handleChange} />
                        </div>
                        <div className="form-group">
                            <label for="exampleFormControlInput1">Name:</label>
                            <input className="form-control" id="exampleFormControlInput1" type="text" value={this.state.value} onChange={this.handleChange} />
                        </div>
                        <div className="text-center">
                        <input className="btn btn-info text-center" type="submit" value="Submit" />

                        </div>
                    </form>
                </div>
            </div>

        );
    }
}
export default NameForm;