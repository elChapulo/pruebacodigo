import React from 'react';
import { Accordion, Card, Button } from 'react-bootstrap';
export default props => {
  return (
    <div className="container d-flex flex-row justify-content-center">
      <div className="card shadow p-4" style={{ 'width': 'fit-content' }}>
        <div className="p-4 text-center">
          <h1>Luis Enrique Alva Villena</h1>
          <h2>Desarrollador de software</h2>
        </div>
        <Accordion>
        <Card>
            <Card.Header>
              <Accordion.Toggle as={Button} variant="link" eventKey="0">
                <h4>Información Personal:</h4>
              </Accordion.Toggle>
            </Card.Header>
            <Accordion.Collapse eventKey="0">
              <Card.Body>
                <ul>
                  <li><b>Fecha de Nacimiento</b> - 28 Feb 1990</li>
                  <li><b>Pais</b> - Perú</li>
                  <li><b>Educacion</b> - PUCP / UTP</li>

                </ul>

              </Card.Body>
            </Accordion.Collapse>
          </Card>          
          <Card>
            <Card.Header>
              <Accordion.Toggle as={Button} variant="link" eventKey="1">
                <h4>Experiencia Laboral:</h4>
              </Accordion.Toggle>
            </Card.Header>
            <Accordion.Collapse eventKey="1">
              <Card.Body>
                <ul>
                  <li><b>Wego S.A.C</b> - CTO/Desarrollador Frontend</li>
                  <li><b>Business Technology Group S.A</b> - Desarrollador Python</li>
                  <li><b>Ruta Norte</b> - CTO/Desarrollador Frontend</li>
                  <li><b>Rio Pacífico</b> - Arquitecto de Softaware</li>
                  <li><b>Yaroslab</b> - Desarrollador Python</li>
                </ul>

              </Card.Body>
            </Accordion.Collapse>
          </Card>
          <Card>
            <Card.Header>
              <Accordion.Toggle as={Button} variant="link" eventKey="2">
                <h4>Habilidades Informaticas:</h4>
              </Accordion.Toggle>
            </Card.Header>
            <Accordion.Collapse eventKey="2">
              <Card.Body>
                <ul>
                  <li>Lenguajes</li>
                  <ul>
                    <li>Javascript</li>
                    <li>Python</li>
                    <li>PHP</li>
                    <li>Java</li>
                  </ul>
                  <li>Frameworks</li>
                  <ul>
                    <li>Vuejs</li>
                    <li>ReactJs</li>
                    <li>Flask</li>
                    <li>Spring</li>
                  </ul>
                  <li>Base de datos</li>
                  <ul>
                    <li>Postgresql</li>
                    <li>Mysql</li>
                    <li>MariaDB</li>
                  </ul>
                  <li>Docker</li>
                  <li>Linux</li>
                  <li>Versionamiento Git</li>
                </ul>
              </Card.Body>
            </Accordion.Collapse>
          </Card>
        </Accordion>


      </div>

    </div>
  );

};