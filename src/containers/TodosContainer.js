import React from 'react'
import { TodoList, AddTodo } from "../components";


class TodosContainer extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            todos: []
        }
    }

    componentDidMount() {
        // Get todos (from api, for example)
        this.setState({
            todos: [
            ]
        })
    }

    onDoneHandler = (index, event) => {
        const todos = this.state.todos.slice(0);
        todos[index].done = event.target.checked;
        this.setState({ todos });
    }

    onDeleteHandler = index => {
        const todos = this.state.todos.slice(0);
        todos.splice(index, 1);
        this.setState({ todos });
    }

    onAddTodoHandler = newTodo => {
        const todos = this.state.todos.slice(0);
        todos.push(newTodo);
        this.setState({ todos });
    }

    render() {
        return (
            <div className="row d-flex flex-column">
                <div className="p-4">
                    <h1 className="text-center">Control de tareas</h1>
                </div>
                <div className="d-flex flex-row">
                    <div className="col-sm d-flex flex-column justify-content-center">
                        <TodoList todos={this.state.todos}
                            onDoneHandler={this.onDoneHandler}
                            onDeleteHandler={this.onDeleteHandler} />
                    </div>
                    <div className="col-sm d-flex flex-column justify-content-center">
                        <AddTodo onAddTodoHandler={this.onAddTodoHandler} />
                    </div>
                </div>



            </div>
        )
    }
}

export default TodosContainer