import React, { Component } from 'react';
import logo from './logo.svg';
import { Navbar } from "./components";
import { Home, Experience, TodoPage } from "./pages";
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
//import SideBar  from "./components/sidebar";
import './App.css';

class App extends Component {
  render() {
    return (
      <div className=" ">
        <Navbar />
        <Router>
          <Route path="/" exact component={Home} />
          <Route path="/experience" exact component={Experience} />
          <Route path="/todolist" exact component={TodoPage} />
        </Router>

      </div>
    );
  }
}

export default App;