// navbar.js

import React from 'react';
import { Navbar, Nav, NavDropdown, Form, FormControl, Button } from 'react-bootstrap';

export default props => {
    return (
<Navbar bg="light" expand="lg">
 
  <Navbar.Brand href="#home"><h3>Codigo de Prueba</h3></Navbar.Brand>
  <Navbar.Toggle aria-controls="basic-navbar-nav" />
  <Navbar.Collapse id="basic-navbar-nav">
    <Nav className="mr-auto">
      <Nav.Link href="/">Datos de postulante</Nav.Link>
      <NavDropdown title="Ejemplos" id="basic-nav-dropdown">
        <NavDropdown.Item href="/todolist">Tareas pendientes</NavDropdown.Item>
      </NavDropdown>
    </Nav>
    <Form inline>
      <FormControl type="text" placeholder="Search" className="mr-sm-2" />
      <Button variant="outline-success">Search</Button>
    </Form>
  </Navbar.Collapse>
</Navbar>
    );
};