import React from 'react'
import PropTypes from 'prop-types'

const Todo = ({ index, text, done, priority, onDoneHandler, onDeleteHandler }) => {

  const todoStyle = {
    'border': '1px solid black',
    padding: '20px',
    'margin-top': '20px'
  }

  return (
    <div className={` ${done ? 'is-done' : ''}`}
      style={todoStyle}>
      <h4 style={{ textDecoration: done ? 'line-through' : 'none' }}>{text}</h4>
      <div>Prioridad: {priority}</div>
      <div className="d-flex flex-column">
        <div className="d-flex flex-row justify-center p-3">
          <input
            type="checkbox"
            className="form-check-input text-center"
            checked={done}
            onChange={event => onDoneHandler(index, event)} />
          <span >
            {done ? 'Completado' : 'Establecer como completado'}
          </span>
        </div>
        <button className="btn btn-info" onClick={() => onDeleteHandler(index)}>Delete</button>
      </div>
    </div>
  )
};

Todo.propTypes = {
  index: PropTypes.number.isRequired,
  text: PropTypes.string.isRequired,
  done: PropTypes.bool,
  priority: PropTypes.number,
  onDoneHandler: PropTypes.func.isRequired,
  onDeleteHandler: PropTypes.func.isRequired
}

Todo.defaultProps = {
  priority: 2
}

export default Todo