import React from 'react'

class AddTodo extends React.Component {
    constructor(props) {
        super(props)

        this.defaultState = {
            text: '',
            priority: 2
        }

        this.state = this.defaultState;
    }

    onChangeHandler = event => {
        const target = event.target;
        this.setState({
            [target.name]: target.type === 'checkbox' ? target.checked : target.value
        });
    }

    onSubmitHandler = event => {
        event.preventDefault();
        this.props.onAddTodoHandler(this.state);
        this.setState(this.defaultState);
    }

    render() {
        const { text, priority } = this.state;
        return (
            <div className="" >
                <div class="card p-5 container">
                    <form className="App" onSubmit={this.onSubmitHandler}>
                        <input placeholder="Ingrese una tarea" 
                            className="form-control m-3" name="text"
                            value={text}
                            onChange={this.onChangeHandler} />
                        <select name="priority" 
                            className="form-control m-3"
                            value={priority}
                            onChange={this.onChangeHandler}>
                            <option value={1}>Alta</option>
                            <option value={2}>Media</option>
                            <option value={3}>Baja</option>
                        </select>
                        <button className="btn btn-info mt-3">Añadir Tarea</button>
                    </form>
                </div>
            </div>
        )
    }
}

export default AddTodo